import {enable} from '../config';
import svg4everybody from 'svg4everybody/dist/svg4everybody.js';

if (enable.components.icons === true) {
    svg4everybody();
}
