let indent = 24;

// Responsive
let mqBreakpoints = [
    ['xs', 679 - indent],
    ['sm', 680 - indent],
    ['md', 984 - indent],
    ['lg', 1224 - indent],
    ['xl', 1440 - indent]
];

// Not responsive
let viewportWidth = 1220;

export {indent, mqBreakpoints, viewportWidth}
